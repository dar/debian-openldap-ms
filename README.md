OpenLDAP packages for Debian
============================

Barebone Debian packages used by automated
[Æ-DIR installation](https://www.ae-dir.com/install.html)
which generates everything else needed to run the OpenLDAP servers.

These packages are not for general use! Packaging can change at any time without further notice.

You have been warned!

DevOps-friendly:

  * no default configuration, not even an example
  * no init file or systemd unit
  * no Berkeley-DB backends back-bdb or back-hdb
  * no valid database directory
  * no dedicated demon user
  * no support for dpkg-configure

Back-port patches applied:

  * LDAP_DEBUG_TRACE for "not indexed" log messages
    (see [ITS#7796](https://bugs.openldap.org/show_bug.cgi?id=7796))
  * RFE: slapo-constraint to return filter used in diagnostic message
    (see [ITS#8866](https://bugs.openldap.org/show_bug.cgi?id=8866))
  * slapd with ppolicy and pwdAccountLockedTime crashes
    (see [ITS#9171](https://bugs.openldap.org/show_bug.cgi?id=9171))
