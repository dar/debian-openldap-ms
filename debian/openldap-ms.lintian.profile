# The default profile for "openldap-ms"
Profile: debian/openldap-ms
# It has all the checks and settings from the "debian" profile
Extends: debian/main

Tags:
  file-in-usr-marked-as-conffile,
  license-problem-non-free-RFC-BCP78,
  license-problem-non-free-RFC,
Overridable: yes
